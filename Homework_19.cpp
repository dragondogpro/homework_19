#include <iostream>
#include <list>

#include "Cat.h"
#include "Dog.h"
#include "Cow.h"

int main()
{
    std::list<Animal*> animals = {new Cat(), new Dog(), new Cow()};

    for (Animal* var : animals)
        var->Voice();
}