#ifndef DOG_H_INCLUDED
#define DOG_H_INCLUDED

#include "Animal.h"

class Dog : public Animal
{
private:
	std::string say = "Woof!";
public:
	Dog() {}

	void Voice() override
	{
		std::cout << say << std::endl;
	}
};
#endif