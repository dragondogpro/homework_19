#ifndef ANIMAL_H_INCLUDED
#define ANIMAL_H_INCLUDED

#include<iostream>

class Animal
{
private:
	std::string say = "define";
public:
	Animal() {}

	virtual void Voice()
	{
		std::cout << say << std::endl;
	}
};
#endif

