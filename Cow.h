#ifndef COW_H_INCLUDED
#define COW_H_INCLUDED

#include "Animal.h"

class Cow :
	public Animal
{
private:
	std::string say = "Moo!";
public:
	Cow() {}
	void Voice() override
	{
		std::cout << say << std::endl;
	}
};
#endif