#ifndef CAT_H_INCLUDED
#define CAT_H_INCLUDED

#include "Animal.h"

class Cat : public Animal
{
private:
	std::string say = "Meow!";
public:
	Cat() {}

	void Voice() override 
	{
		std::cout << say << std::endl;
	}
};
#endif 
